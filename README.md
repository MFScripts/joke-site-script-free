**Jokes Site Script Free**

Create your own jokes site with this free jokes site script. 

**About Jokes Site Script Free**

This jokes site script allows you to create a joke site containing nearly 10,000 jokes.

This is a community based release that is supplied without support or liability. You are free to use the jokes site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Features:**

* Written in php/mysql.
* All pages are created using Apaches mod-rewrite so any recipe page all look like static pages within the search engines.
* Comes with nearly 10,000 funny jokes.
* Jokes managed into over 100 different categories.
* Search feature which searches jokes and categories.
* Submit jokes feature for visitors to add jokes.
* Basic admin area to approve/decline submitted jokes.
* Rating/stars feature for each joke.
* Links to digg, del.icio.us and reddit included on every joke.
* Random Joke feature.
* All content is automatically managed
* i.e. homepage is updated automatically.
* 'Friends' page for ease of management of links exchanges.
* Control the amount of items displayed within search results/browsing via the config.
* Use html code within added jokes.
* Original design fireworks png file included.
* Ideal for adding adsense or other code.

**Requirements**

* php 4.x
* mysql
* mod rewrite module on apache
* HD/BW requirements will vary on your traffic, scripts require about 100kb.

**Installation**

Full instructions are given in the ___SETUP_INSTRUCTIONS.txt file found within the repository.

**Contributions**

If you'd like to contribute to the project, please contact us via the project https://bitbucket.org/MFScripts/joke-site-script-free

**License**

Fonts Site Script Free is copyrighted by http://mfscripts.com and is released under the http://opensource.org/licenses/MIT. You are free to use the jokes site script as you wish. The only restriction to this PHP script is that this README file must not be edited or deleted. 

**Support**

This code is released without any support and as-is. Any support requests on this version will be removed. For community driven support please use the project https://bitbucket.org/MFScripts/joke-site-script-free